<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//For list/add/delete files
Route::post('/add','UserFileController@store');
Route::post('/files', 'UserFileController@index');
Route::delete('/file/{file}','UserFileController@delete');




