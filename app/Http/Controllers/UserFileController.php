<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Image;
use Validator;
use File;

use App\Models\UserFile;

class UserFileController extends Controller
{
    //For listing files
    public function index(Request $request){
        $limit = 5;
        $tab = $request['tab'];
        $files = UserFile::where('deleted',$tab)->where(
            function($q) use ($request){
                if($request->has('searchText') && !empty(request('searchText'))){
                    $q->orWhere('file_name','LIKE','%' . request('searchText') . '%')->orWhere('type','LIKE','%' . request('searchText') . '%');
                }
        })->orderBy('id', 'desc')->paginate($limit);
        return response()->json($files);
    }

    //For deleting file
    public function delete($file)
    {
        $deletedFile = UserFile::find($file);
        $deletedFile->deleted = 1;
        $deletedFile->save();

        return response()->json($deletedFile->id);
    }

    //For storing files
    public function store(Request $request){
        $returnArr = [];
        $validator = Validator::make($request->all(),[
            'file' => 'required|mimes:png,txt,doc,docx,pdf,jpeg,jpg,gif|size:2048',
        ]);
        if(!$validator->fails()){
            $folderPath = "files/";
            $file_tmp = $_FILES['file']['tmp_name'];
            //For getting file extension
            $file_ext = explode('.',$_FILES['file']['name']);

            $fileName = time().'.'.$file_ext[1];
            $file =  $folderPath . $fileName;
            move_uploaded_file($file_tmp, $file);
            $userFile = new UserFile();
            $userFile->file_name = $fileName;
            $userFile->type = $file_ext[1];
            $userFile->save();
            $returnArr['success'] = 1;
            $returnArr['message'] = 'Successfully added';
        }else{
            $returnArr['success'] = 0;
            $returnArr['message'] = $this->errorFormat($validator->messages());
        }
    
        return response()->json($returnArr);
    }
}
