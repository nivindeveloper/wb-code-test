<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

        <link href="/css/app.css" rel="stylesheet">

        <!-- Styles -->
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'base_url' => env("BASE_URL"),
                //'client_secret' => env("PASSWORD_CLIENT_SECRET")
            ]) !!};
        </script>
    </head>
    <body>
        <div id="app">
        </div>
        <script src="{{mix('js/app.js')}}" ></script>
    </body>
</html>
