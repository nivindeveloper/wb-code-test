import axios from "axios";
import  {GET_FILES, FILE_ERROR, ADD_FILE, DELETE_FILE} from "./types";

//Get all file list
export const getFiles = (formData) => async dispatch => 
{
    const config = {
        headers: {
            "Content-Type": "application/json"
        }
    };
    try {
        const res = await axios.post("/api/files?page="+formData.page, formData, config);
        dispatch({
            type: GET_FILES,
            payload: res.data
        });
    } catch (error) {
        dispatch({
            type: POST_ERROR,
            payload:{
                msg: error.response.statusText,
                status: error.response.status
            }
        });
    }
}

//Delete File
export const deleteFile = fileId => async dispatch => 
{
    try {
        await axios.delete(`/api/file/${fileId}`);
        dispatch({
            type: DELETE_FILE,
            payload: fileId
        })
    } catch (err) {
        dispatch({
            type: FILE_ERROR,
            payload:{
                msg: err.response.statusText,
                status: err.response.status
            }
        })
    }
}