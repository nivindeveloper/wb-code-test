import React, {Component} from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import Header from './Header';
import Footer from './Footer';
import HomePage from './HomePage';
import Add from './Add';

export default class AppRouter extends React.Component{
    render() {
        return (
            <BrowserRouter>
                <Container fluid>
                    <Header/>
                    <Switch>
                        <Route path="/" component={HomePage} exact={true} />
                        <Route path="/add" component={Add} exact={true}/>
                    </Switch>
                    <Footer/>
                </Container>
            </BrowserRouter>
        );
    }
}
