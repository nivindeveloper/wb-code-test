import React from "react";
import {connect} from "react-redux";
import { Button, Image } from 'react-bootstrap';
import Moment from 'moment';

import { deleteFile } from '../actions/files';

//Public file path
const path = "/files/";
const fileTypes = ["txt", "doc", "docx", "pdf"];

const UserFileItems = ({file,deleteFile,tab}) => {
    return (
        <>
        <tr>
            <td>
                {
                    fileTypes.indexOf(file.type) > -1 ? (
                        <a href={path+file.file_name}>{file.file_name}</a>
                    ) : (
                        <Image src={path+file.file_name} thumbnail width="100" height="100"/>
                    )
                }
            </td>
            <td>{file.type}</td>
            <td>{Moment(file.created_at).format('DD-MM-YYYY')}</td>
            {tab == 0 ? (
                <td>
                    <Button onClick={()=>deleteFile(file.id)} variant="danger">Delete</Button>
                </td>
            ) : null
            }
        </tr>
        </>
    )
};

export default connect(null,{deleteFile})(UserFileItems);