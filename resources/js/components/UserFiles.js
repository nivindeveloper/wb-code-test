import React, { Component } from "react";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import { Button, Card, Table, Nav, Form, FormControl } from 'react-bootstrap';

import { getFiles, deleteFile } from "../actions/files";
import UserFileItems from "./UserFileItems";

class UserFiles extends Component  {

    constructor() {
        super();
        this.state = {
            activeKey : 0,
            searchText: "",
            pageNumber: 1
        }
    }

    componentDidMount() {
        this.props.getFiles({"tab":0, "page":this.state.pageNumber});
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.files.current_page > 1 && this.props.files.data.length == 0) {
            this.props.getFiles({"tab":this.state.activeKey, searchText:this.state.searchText, "page":this.props.files.current_page-1});
        }
    }

    removeFile(id) {
        this.props.deleteFile(id);
    }

    handleSelect = (eventKey) => {
        this.setState({activeKey:eventKey});
        this.props.getFiles({"tab":eventKey, "page":this.state.pageNumber});
    };

    handleInputChange(event) {
        this.setState({
            searchText: event.target.value,
        });
    }

    handleSearch() {
        this.props.getFiles({"tab":this.state.activeKey, searchText:this.state.searchText});
    }

    handlePageChange(pageNumber) {
        const {current_page, per_page, total} = this.props.files;

        this.props.getFiles({"tab":this.state.activeKey, searchText:this.state.searchText, "page":pageNumber});
    }

    render() {
        const {current_page, per_page, total} = this.props.files;
        const {activeKey} = this.state;

        return (
            <>
                <Nav variant="pills" activeKey={this.state.activeKey} onSelect={this.handleSelect} className="mt-5 mb-1">
                    <Nav.Item>
                        <Nav.Link eventKey="0">
                            Current
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="1" title="Item">
                            History
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="form-inline">
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" value={this.state.searchText} onChange={e => this.handleInputChange(e)}/>
                        <Button variant="outline-success" onClick={()=>this.handleSearch()}>Search</Button>
                    </Nav.Item>
                </Nav>
                {
                    (this.props.files.hasOwnProperty("data") && this.props.files.data.length > 0) ? (
                        <>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                    <th>File Name</th>
                                    <th>Type</th>
                                    <th>Created</th>
                                    {
                                        activeKey == 0 ? (
                                            <th>Action</th>
                                        ) : null
                                    }
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.files.data.map(file=>
                                        <UserFileItems key={file.id} file={file} tab={activeKey}/>
                                    )}
                                </tbody>
                            </Table>
                            <Pagination
                                activePage={current_page}
                                itemsCountPerPage={per_page}
                                totalItemsCount={total}
                                onChange={this.handlePageChange.bind(this)}
                                itemClass="page-item"
                                linkClass="page-link"
                                firstPageText="First"
                                lastPageText="Last"
                            />
                        </>
                    ) : (
                        <Card>
                            <Card.Body>No records found.</Card.Body>
                        </Card>
                    )
                }
            </>
        )
    }
}

const mapStateToProps = state =>  ({
    files: state.files.files
})

const mapDispatchToProps = (dispatch) => {
    return {
        getFiles: (data) => {
            dispatch(getFiles(data));
        },
        deleteFile: (id) => {
            dispatch(deleteFile(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserFiles);