import React from "react";
import { Alert } from 'react-bootstrap';

import axios from 'axios';

class Add extends React.Component{

    constructor(){
        super();
        this.state = {
            selectedFile:'',
            errorMsg: "",
            successMsg: "",
        }

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    //Set the input value
    handleInputChange(event) {
        this.setState({
            selectedFile: event.target.files[0],
            errorMsg: "",
            successMsg: ""
        })
    }

    submit(){
        //Assign submitted form data
        const data = new FormData();
        data.append('file', this.state.selectedFile)
        let url = "/api/add";

        axios.post(url, data, {})
        .then(res => {
            //If the response is success show the success message and redirect to home page
            if(res.data.success){
                this.setState({successMsg:res.data.message, selectedFile:''});
                setTimeout(() => {
                    window.location = "/";
                }, 1000);
            }else{
                this.setState({errorMsg:res.data.message});
            }
        })
    }

    render(){
        return(
            <div>
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        {
                            this.state.successMsg != "" ? (
                                <Alert variant="success">{this.state.successMsg}</Alert>
                            ) : null
                        }
                        <h3>File Upload</h3>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Select File :</label>
                                <input type="file" className="form-control" name="upload_file" onChange={this.handleInputChange} />
                                {
                                    this.state.errorMsg != "" ? (
                                        <p className="text-danger">{this.state.errorMsg}</p>
                                    ) : null
                                }
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-6">
                                <button type="submit" className="btn btn-dark" onClick={()=>this.submit()}>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Add;