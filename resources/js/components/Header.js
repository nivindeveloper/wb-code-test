import React, {Component} from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import {  Link } from 'react-router-dom';

class Header extends Component{
    render() {
        return (
            <Navbar bg="light" expand="md" sticky="top" >
                <Navbar.Brand><Link to="/">Code Test</Link></Navbar.Brand>
                <Nav.Item>
                    <Link to="/add">ADD</Link>
                </Nav.Item>
            </Navbar>
        );
    }
}

export default Header;